module.exports = {
  extends: 'stylelint-config-standard',
  plugins: [
    'stylelint-declaration-strict-value',
    'stylelint-order',
  ],
  rules: {
    'number-leading-zero': 'never',
    'at-rule-no-unknown': null,
    // 'color-named': 'never',
    'no-descending-specificity': null,

    'scale-unlimited/declaration-strict-value': [
      [
        'z-index', 'font-family', 'fill', 'stroke',
      ],
    ],

    'order/order': [
      {
        type: 'at-rule',
        name: 'include',
      },
      'declarations',
      {
        type: 'at-rule',
        name: 'media',
      },
      'rules',
    ],
    'order/properties-alphabetical-order': true,
  },
};
