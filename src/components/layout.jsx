import React from 'react';
import Navigation from './Navigation';
import Footer from './Footer';

const layout = ({ children }) => (
  <>
    <Navigation />
    <main>
      {children}
    </main>
    <Footer />
  </>
);

export default layout;
