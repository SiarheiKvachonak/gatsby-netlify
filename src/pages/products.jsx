import React from 'react';
import { graphql, Link } from 'gatsby';
import Image from 'gatsby-image';
import Layout from '../components/layout';
import styles from '../styles/products.module.css';

const ComponentName = ({ data }) => {
  const { allContentfulProduct: { nodes: products } } = data;

  return (
    <Layout>
      <section className={styles.page}>
        {products.map(({
          title, price, image, id, slug,
        }) => (
          <article key={id}>
            <Image alt={title} fluid={image.fluid} />
            <h3>
              {title}
              {' '}
              <span>
                {price}
                $
              </span>
            </h3>
            <Link to={`/products/${slug}`}>more details</Link>
          </article>
        ))}
        <h1>hello from products</h1>
      </section>
    </Layout>
  );
};

export const query = graphql`
  {
    allContentfulProduct {
      nodes {
        title
        price
        id
        slug
        image {
          fluid {
            ...GatsbyContentfulFluid
          }
        }
      }
    }
  }
`;

export default ComponentName;
