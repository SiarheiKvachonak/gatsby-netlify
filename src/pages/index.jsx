import React from 'react';
import Layout from '../components/layout';

const IndexPage = () => (
  <Layout>
    <div>
      Hello
    </div>
  </Layout>
);

export default IndexPage;
