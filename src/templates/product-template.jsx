import React from 'react';
import { graphql, Link } from 'gatsby';
import Image from 'gatsby-image';
import Layout from '../components/layout';
import styles from '../styles/singleProduct.module.css';

const pruductTemplate = ({ data: { product } }) => {
  const {
    price, title, image, info,
  } = product;

  return (
    <Layout>
      <div className={styles.singleProduct}>
        <div>
          <Link to="/products">back to products</Link>
          <h1>
            single product:
            {title}
          </h1>
        </div>
        <section className="single-product-content">
          <article>
            <Image alt={title} fixed={image.fixed} />
          </article>
          <article>
            <h1>{title}</h1>
            <div>{info.info}</div>
            <div>
              {price}
              $
            </div>
          </article>
        </section>
      </div>
    </Layout>
  );
};

export const query = graphql`
query getSinglePruduct($slug: String) {
  product: contentfulProduct(slug: {eq: $slug}) {
    price
    title
    image {
      fixed (width: 300){
        ...GatsbyContentfulFixed
      }
    }
    info {
      info
    }
  }
}`;

export default pruductTemplate;
